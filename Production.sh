rm -rf ~/Production
cp -r ~/Staging/ ~/Production/
cd ~/Production
chmod -R 777 var/cache/
chmod -R 777 var/log/
sed -i 's/8097/8096/' docker-compose.yml
sed -i 's/33062/33063/' docker-compose.yml
docker-compose down
docker-compose exec fpm composer install
composer install
docker-compose up --build -d