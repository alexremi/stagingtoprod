rm -rf ~/Staging
git clone git@bitbucket.org:alexremi/stagingtoprod.git ~/Staging
cd ~/Staging
sed -i 's/8099/8097/' docker-compose.yml
sed -i 's/33061/33062/' docker-compose.yml
docker-compose down
docker-compose exec fpm composer install
composer install
docker-compose up --build -d

